// Our Amazing JS.

(function ($, Drupal) {
  Drupal.behaviors.jokenpoGame = {
    attach: function (context, settings) {

      // If there is no an element, return false.
      if ($('.block-jchampion-block').html() == null) {
        return false;
      }

      // If the element is equal a computer, so change color to red.
      var elementChampion = $('.block-jchampion-block').html();

      if (elementChampion.includes('Computer')) {
        $('.js-jokenpo-champion').css('color', 'red');
      }
      else {
        $('.js-jokenpo-champion').css('color', 'green');
      }


    }
  };
})(jQuery, Drupal);

