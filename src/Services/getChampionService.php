<?php

namespace Drupal\jokenpo\Services;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service to get the highest score of the game Jokenpo.
 */
class GetChampionService {
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct to dependency injection of the config.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Function that returns the player that have the highest score.
   */
  public function getChampion() {

    $champion = '';

    $user_name = 'Player';

    $config = $this->configFactory->get('jokenpo.settings');
    $playerScore = $config->get('player_score');
    $computerScore = $config->get('computer_score');

    if ($playerScore > $computerScore) {
      $champion = $user_name;
    }

    elseif ($playerScore < $computerScore) {
      $champion = (string) $this->t('Computer');
    }
    else {
      $champion = (string) $this->t('Nobody');
    }
    return $champion;
  }

}
