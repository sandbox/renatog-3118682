<?php

namespace Drupal\jokenpo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\jokenpo\Service\JokenpoService;

/**
 * Jokenpo Class.
 */
class JokenpoScoreController extends ControllerBase {

  /**
   * This variable will be used on dependency injection with config object.
   *
   * @var config
   */
  public $config;

  /**
   * This variable will be used on dependency injection.
   *
   * @var project
   */
  public $project;

  /**
   * This variable will be used on dependency injection of Jokenpo.
   *
   * @var jokenpoService
   */
  public $jokenpoService;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandler $project, JokenpoService $jokenpoService) {
    $this->config = $config_factory;
    $this->project = $project;
    $this->jokenpoService = $jokenpoService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('jokenpo.ranking')
    );
  }

  /**
   * Function to get view of page.
   */
  public function getPage() {

    $ranking = $this->jokenpoService->getRanking();

    $playerName = $this->t('Player');

    if ($this->project->moduleExists('training')) {
      $configTraining = $this->config('training.settings');
      $playerName = $configTraining->get('full_name');
    }

    return [
      '#theme' => 'jokenpo-scores',
      '#player_name' => $playerName,
      '#player_score' => $ranking['player_score'],
      '#computer_score' => $ranking['computer_score'],
      '#tie_score' => $ranking['tie_score'],
    ];

  }

}
