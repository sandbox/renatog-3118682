<?php

namespace Drupal\jokenpo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * The controller class responsible for showing the user the "About-us" page.
 */
class AboutUsController extends ControllerBase {

  /**
   * Method used to return the 'About-us page'.
   */
  public function getPage() {
    $markup = 'Daniel <br>';
    $markup .= 'Caio <br>';
    $markup .= 'Mateus <br>';
    $markup .= 'Renato';
    return [
      '#type' => 'markup',
      '#markup' => $markup,
    ];

  }

}
