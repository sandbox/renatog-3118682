<?php

namespace Drupal\jokenpo\Form;

use Drupal\user\Entity\User;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class Name.
 */
class JokenpoGameForm extends ConfigFormBase {
  public const PLAYER_WIN = 0;
  public const COMPUTER_WIN = 1;
  public const TIE = 2;
  public const ROCK = 'Rock';
  public const PAPER = 'Paper';
  public const SCISSORS = 'Scissors';

  /**
   * This variable will be used on dependency injection.
   *
   * @var project
   */
  public $project;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param Drupal\Core\Extension\ModuleHandlerInterface $project
   *   The project variable to use in depency injection.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $project) {
    parent::__construct($config_factory);
    $this->project = $project;
  }

  /**
   * Use services in class container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('config.factory'),
        $container->get('module_handler')
    );
  }

  /**
   * Set the Form ID.
   */
  public function getFormId() {
    return 'jokenpo_admin_settings';
  }

  /**
   * Get Editable Config Names.
   */
  protected function getEditableConfigNames() {
    return [
      'jokenpo.settings',
    ];
  }

  /**
   * Default Method to Construct Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('jokenpo.settings');

    $handDefaultValue = $config->get('hand');
    $computerScore = $config->get('computer_score') ? $config->get("computer_score") : 0;
    $playerScore = $config->get('player_score') ? $config->get("player_score") : 0;
    $tieScore = $config->get('tie_score') ? $config->get("tie_score") : 0;

    $playerName = 'Player';

    if ($this->project->moduleExists('training')) {
      $configTraining = $this->config('training.settings');
      if (!empty($configTraining->get('full_name'))) {
        $playerName = $configTraining->get('full_name');
      }
    }

    $markup = $this->t('@player_name = @player_score', ['@player_name' => $playerName, '@player_score' => $playerScore]) . '<br />' . $this->t('Computer = @computer_score', ['@computer_score' => $computerScore]);
    $markup .= '<br />' . $this->t('Tie = @tie_score', ['@tie_score' => $tieScore]);
    $form['#markup'] = $markup;

    $form['hand'] = [
      '#title' => $this->t("Choose your Jokenpo hand"),
      '#type' => 'radios',
      '#required' => TRUE,
      '#default_value' => $handDefaultValue,
      '#options' => [
        self::ROCK => $this->t('Rock'),
        self::PAPER => $this->t('Paper'),
        self::SCISSORS => $this->t('Scissors'),
      ],

    ];

    // Compares your hand with the computer's to settle the winner of the match.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Showdown'),
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => ['::resetScoreBoardSubmit'],
    ];

    // Adding our Javascript library.
    $form['#attached']['library'][] = 'jokenpo/jokenpo-game';

    return $form;
  }

  /**
   * This function will update the score board.
   */
  public function updateScoreBoard(int $showdown) {
    $config = $this->config('jokenpo.settings');

    switch ($showdown) {
      case 0:
        $config->set('player_score', (int) $config->get('player_score') + 1);
        $config->save();
        break;

      case 1:
        $config->set('computer_score', (int) $config->get('computer_score') + 1);
        $config->save();
        break;

      case 2:
        $config->set('tie_score', (int) $config->get('tie_score') + 1);
        $config->save();
        break;
    }
  }

  /**
   * This method will get the computer plays using randon approach.
   */
  public function getComputerPlays() {
    $random_number = rand(0, 2);
    $play = '';
    switch ($random_number) {
      case 0:
        $play = self::ROCK;
        break;

      case 1:
        $play = self::PAPER;
        break;

      case 2:
        $play = self::SCISSORS;
        break;
    }
    return $play;
  }

  /**
   * Compares the user's hand with the computer's to declare the winner.
   *
   * @return int
   *   Return=0 the winner is the player
   *   return=1 the winner is the computer
   *   return=2 it's a tie.
   */
  public function showdown(string $hand, string $computerPlay) {
    switch ($hand) {
      case "Rock":
        if ($computerPlay == self::ROCK) {
          return self::TIE;
        }
        if ($computerPlay == self::PAPER) {
          return self::COMPUTER_WIN;
        }
        if ($computerPlay == self::SCISSORS) {
          return self::PLAYER_WIN;
        }

        break;

      case "Paper":
        if ($computerPlay == self::ROCK) {
          return self::PLAYER_WIN;
        }
        if ($computerPlay == self::PAPER) {
          return self::TIE;
        }
        if ($computerPlay == self::SCISSORS) {
          return self::COMPUTER_WIN;
        }

        break;

      case "Scissors":
        if ($computerPlay == self::ROCK) {
          return self::COMPUTER_WIN;
        }
        if ($computerPlay == self::PAPER) {
          return self::PLAYER_WIN;
        }
        if ($computerPlay == self::SCISSORS) {
          return self::TIE;
        }

        break;
    }

  }

  /**
   * Show the result message to the end user.
   */
  public function showMessage($showdown, $computerPlay) {
    switch ($showdown) {
      case 0:
        $this->messenger()->addMessage($this->t('The computer plays @computer_play', ["@computer_play" => $computerPlay]) . $this->t('@blank_space@... You win!', ['@blank_space@' => ' ']), 'status');

        break;

      case 1:
        $this->messenger()->addMessage($this->t('The computer plays @computer_play', ["@computer_play" => $computerPlay]) . $this->t('@blank_space@... You lose!', ['@blank_space@' => ' ']), 'error');
        break;

      case 2:
        $this->messenger()->addMessage($this->t('The computer plays @computer_play', ["@computer_play" => $computerPlay]) . $this->t('@blank_space@... Draw!', ['@blank_space@' => ' ']), 'warning');
        break;

    }
  }

  /**
   * Default Method to Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get Config Object.
    $config = $this->config('jokenpo.settings');

    $hand = $form_state->getValue('hand');

    $config->set('hand', $hand);
    $config->save();

    $computerPlay = $this->getComputerPlays();
    $showdown = $this->showdown($hand, $computerPlay);
    $this->updateScoreBoard($showdown);
    $this->showMessage($showdown, $computerPlay);
    $this->jokeponMaster();

  }

  /**
   * This function will clear our score board.
   */
  public function resetScoreBoardSubmit(array &$form, FormStateInterface $form_state) {

    $config = $this->config('jokenpo.settings');
    $scores = ['player_score', 'computer_score', 'tie_score'];
    foreach ($scores as $value) {
      $config->set($value, 0);
    }
    $config->save();

    $this->messenger()->addMessage($this->t('Scoreboard successfully reset.'), 'status');

  }

  /**
   * Check user option on login and redirect user if necessary.
   */
  public static function checkJokenpoOption(array $form, FormStateInterface $form_state) {

    $jokenpoOption = $form_state->getValue('jokenpo_option');

    if (!empty($jokenpoOption)) {

      // Get route.
      $path = Url::fromRoute('jokenpo.game')->toString();

      // Redirect to Jokenpo route.
      $response = new RedirectResponse($path);
      $response->send();
      exit;

    }

  }

  /**
   * If the user has won 10 times, gives it the "jokenpomaster" role.
   */
  public function jokeponMaster() {
    $playerScore = $this->config('jokenpo.settings')->get('player_score');
    if ($playerScore == '10') {
      $roles = user_role_names();
      if (empty($roles['jokenpomaster'])) {
        // Data array.
        $data = ['id' => 'jokenpomaster', 'label' => 'JokenpoMaster'];
        // Creating your role.
        $role = Role::create($data);
        // Saving your role.
        $role->save();
      }
      // We will be not using dependency injection for now, as we don't have enough time @codingStandardsIgnoreLine.
      $user = User::load(\Drupal::currentUser()->id());
      $user->addRole('jokeponmaster');
      $user->save();
    }
  }

}
